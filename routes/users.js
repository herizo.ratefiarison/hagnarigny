var express = require('express');
const userController = require('../controllers/user.controller');
const userValidator = require('../middlewares/validators/user.validator')
const loginValidator = require('../middlewares/validators/login.validator');
const resetValidator = require('../middlewares/validators/reset.validator')
const { guard } = require('../middlewares/guard');
const sendResetMail = require('../middlewares/services/email.service');
var router = express.Router();

/* GET users listing. */
router.get('/login', (req, res) => {
    res.render('login');
});

/*POST single user */

router.post('/login', loginValidator, userController.login);

//REset password

router.get('/forgot-password', (req, res) => {
    res.render('forgot-password');

})



router.post('/forgot-password', userController.resetPassword, sendResetMail);


/*REset password*/
router.get('/reset-password/token', userController.resetPasswordForm);


router.post('/reset-password/token', resetValidator, userController.postResetPassword);





/*sign up*/
router.get('/signup', (req, res) => {
    res.render('signup');
});
router.post('/signup', userValidator, userController.signup);


//route dashboard

router.get('/dashboard', guard, (req, res) => {
    res.render('dashboard');
})

router.post('/save-profile', userController.saveProfile);


/*logout*/

router.get('/logout', (req, res) => {

    req.logout();
    req.flash('success', 'Vous êtes maintenant deconnectés');
    res.redirect('/');

})


module.exports = router;
//export var usersRouter = require('./routes/users');