var express = require('express');
const req = require('express/lib/request');
const res = require('express/lib/response');
const categoryController = require('../controllers/category.controller');
const articleController = require('../controllers/article.controller');
const multerConfig = require('../middlewares/multer.config');
const articleValidator = require('../middlewares/validators/article.validator');
const { guard } = require('../middlewares/guard');

var router = express.Router();
const Article = require('../models/article.model');
const categoryValidator = require('../middlewares/validators/category.validator');

/* GET home page. */
router.get('/', articleController.listArticle);

/*GET Article details */

router.get('/article/:id', articleController.showArticle);

/*GET add Article*/
router.get('/add-article', guard, articleController.addArticle);

/*POST single Article*/

router.post('/add-article', multerConfig, articleValidator, guard, articleController.addOneArticle);


router.get('/edit-article/:id', guard, articleController.editArticle);

router.post('/edit-article/:id', multerConfig, guard, articleController.editOneArticle);

router.get('/delete-article/:id', guard, articleController.deleteArticle);

router.get('/add-category', guard, (req, res) => {
    res.render('add-category');
});

router.post('/add-category', guard, categoryValidator, categoryController.addCategory);





module.exports = router;