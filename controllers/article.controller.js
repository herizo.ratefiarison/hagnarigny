const res = require("express/lib/response");
const { rawListeners } = require("../app");
const Article = require("../models/article.model");
const Category = require("../models/category.model");
const User = require("../models/user.model");

const fs = require("fs");

exports.listArticle = (req, res) => {
    Article.find()
        .then((articles) => {
            req.flash("success", "Test OK");
            res.render("index", { title: "Ndao Hagnarigny", articles: articles });
            //res.status(200).json(articles)
        })
        .catch((err) => {
            res.status(200).json(err);
        });
};
exports.showArticle = (req, res) => {
    Article.findOne({
            _id: req.params.id,
        })
        .then((article) => {
            res.render("single-article", { article: article });
            //console.log(article);
        })
        .catch((err) => {
            res.redirect("/");
            //console.log(err);
        });
};

exports.addArticle = (req, res) => {
    Category.find()
        .then((categories) => {
            res.render("add-article", {
                categories: categories,
            });
        })
        .catch(() => {
            res.redirect("/");
        });
};
exports.addOneArticle = (req, res) => {
    var article = new Article({
        ...req.body,
        image: `${req.protocol}://${req.get("host")}/images/articles/${
      req.file.filename
    }`,
        author: req.user,
        publishiedAt: Date.now(),
    });

    console.log(article);
    //return;

    article.save((err, article) => {
        if (err) {
            req.flash(
                "error",
                "Sorry, an error has occued. Thank you try again later"
            );
            return res.redirect("/add-article");
        }

        User.findOne({ username: req.user.username }, (err, user) => {
            if (err) {
                console.log(err.message);
            }
            user.articles.push(article);
            user.save();
        });
        req.flash("success", "Thank you, Your article has been added");
        return res.redirect("/add-article");

        // if (err) {
        //     Category.find()
        //         .then((categories) => {
        //             res.render('add-article', { categories: categories, error: "Sorry, an error has occued. Thank you try again later" });
        //         })
        //         .catch(() => {
        //             res.redirect('/');
        //         })

        // } else {
        //     Category.find()
        //         .then((categories) => {
        //             res.render('add-article', { categories: categories, success: "Thank you, your article has been added" })

        //         })
        //         .catch(() => {
        //             res.redirect('/');
        //         })

        // }
    });
    /*.then(() => {
          res.render('add-article', { success: "Thank you, your article has been added" })
      })
      .catch(() => {
          res.render('add-article', { error: "Sorry, an error has occued. Thank you try again later" })
      })*/

    //console.log(article);
};
exports.editArticle = (req, res) => {
    const id = req.params.id;
    Article.findOne({
        _id: id,
        author: req.user.id
    }, (err, article) => {
        if (err) {
            req.flash("error", err.message);
            return res.redirect("/");
        }

        if (!article) {
            req.flash("error", 'Sorry, you can\'t modify this article !');
            return res.redirect("/");

        }


        Category.find((err, categories) => {
            if (err) {
                req.flash("error", err.message);
                return res.redirect("/");
            }
            return res.render("edit-article", {
                categories: categories,
                article: article,
            });
        });
    });
};
(exports.editOneArticle = (req, res) => {
    const id = req.params.id;

    Article.findOne({ _id: id, author: req.user._id }, (err, article) => {
        if (err) {
            req.flash("error", err.message);
            return res.redirect("/edit-article/" + id);
        }

        if (!article) {
            req.flash("error", 'Sorry, you can\'t modify this article !');
            return res.redirect("/");

        }

        if (req.file) {
            const filename = article.image.split("/articles/")[1];
            fs.unlink(`public/images/articles/${filename}`, () => {
                console.log("Deleted: " + filename);
            });
        }

        article.name = req.body.name ? req.body.name : article.name;
        article.Category = req.body.Category ? req.body.Category : article.category;
        article.content = req.body.content ? req.body.content : article.content;
        article.image = req.file ?
            `${req.protocol}://${req.get("host")}/images/articles/${
          req.file.filename
        }` :
            article.image;

        article.save((err, article) => {
            if (err) {
                req.flash(
                    "error",
                    "Une erreur s'est produite, veuillez reessayer plus tard"
                );
                return res.redirect("/edit-article/" + id);
            }

            req.flash("success", "Cool, the article has been edited!");
            return res.redirect("/edit-article/" + id);
        });
    });
}),
(exports.deleteArticle = (req, res) => {
    Article.deleteOne({ _id: req.params.id, author: req.user._id },
        (err, message) => {
            if (err) {
                req.flash("error", "Sorry you can not delete this article");
                return res.redirect("/users/dashboard");
            }

            if (message.deleteCount = 0) {
                req.flash('error', 'Sorry, something want wrong');
                return res.redirect("/");
            }

            req.flash("Success", "The article has been deleted!");
            return res.redirect("/users/dashboard");
        }
    );
});