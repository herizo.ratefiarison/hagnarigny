const req = require("express/lib/request");
const Category = require("../models/category.model");

exports.addCategory = (req, res) => {
    const newCategory = new Category({
        ...req.body
    });

    newCategory.save((err, category) => {
        if (err) {
            console.log(err.message);
        }

        req.flash('success', 'Great, Category has been Added!');
        res.redirect('/add-category');

    })
}