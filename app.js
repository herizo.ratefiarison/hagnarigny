var createError = require("http-errors");
var express = require("express");
var session = require("express-session");
var flash = require("connect-flash");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const passport = require("passport");
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
const User = require("./models/user.model");
const bodyParser = require("body-parser");
const Article = require('./models/article.model');

var app = express();

//Gestion de session
app.use(
    session({
        secret: "fkfkfkflldlkrlkjxfkkkylkeei",
        resave: false,
        saveUninitialized: false,
        cookie: {
            secure: false,
        },
    })
);

//Prise en charge JSON

app.use(bodyParser.json());

//Prise en charge des Formulaires HTML
app.use(bodyParser.urlencoded({ extended: false }));

//Connect to the mongo database

const mongoose = require("mongoose");
const { link } = require("fs");
mongoose
    .connect("mongodb://localhost:27017/blog")
    //.connect('mongodb+srv://herizo:<fanomezana#123?>@hagnarigny.tsck2.mongodb.net/blog?retryWrites=true&w=majority')
    .then(() => console.log("Connexion à MongoDB réussie"))
    .catch(() => console.log("Echec de connexion à mongoDB"));

//Add Articles on database
/*
for (let index = 0; index < 8; index++) {
    var article = new Article({
            name: "Qu\'est ce que le Lorom Ipsum",
            content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            publishiedAt: Date.now()
        })
        
            article.save()
                .then(() => console.log("Sauvegarde Article Reussie"))
                .catch(() => console.log("Sauvegarde Article échouée"));
                

} */

//Add Category
/*
for (let index = 1; index < 6; index++) {
    var category = new Category({
        title: "Category " + index,
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    })

    category.save()
        .then(() => console.log("Sauvegarde Categorie Reussie"))
        .catch(() => console.log("Sauvegarde Categorie échouée"));

}*/

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "twig");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

//initialisation flash

//app.use(express.cookieParser('keyboard cat'));
//app.use(express.session({ cookie: { maxAge: 60000 } }));
app.use(flash());

//use passport for inscription

app.use(passport.initialize());
app.use(passport.session());

//configuration passport local mongoose

// CHANGE: USE "createStrategy" INSTEAD OF "authenticate"
passport.use(User.createStrategy());

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use((req, res, next) => {
    if (req.isAuthenticated()) {
        Article.find({ author: req.user._id }, (err, articles) => {
            if (err) {
                console.log(err.message);
            } else {
                console.log(articles)

                res.locals.articles = articles;

            }

            next();
        })

    } else {
        next();
    }
})

app.use((req, res, next) => {
    if (req.user) {
        res.locals.user = req.user;
    }
    res.locals.warning = req.flash('warning');
    res.locals.error = req.flash('error');
    res.locals.success = req.flash('success');
    res.locals.errorForm = req.flash('errorForm');

    next();
});

app.use("/", indexRouter);
app.use("/users", usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
});

module.exports = app;