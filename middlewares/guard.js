exports.guard = (req, res, next) => {
    if (!req.user) {
        req.flash('warning', 'Désolé, vous êtes deconnectés');
        return res.redirect('/users/login');
    }

    next();
}