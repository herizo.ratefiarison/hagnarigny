const nodemailer = require("nodemailer");
const sendResetMail = (req, res, next) => {
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 465,
        secure: true,
        auth: {
            user: "ndao.hagnarigny@gmail.com",
            pass: "nosybehellville",
        },
    });

    var message = "<br>Message: " + req.body.message;
    var mailOptions = {
        from: "ndao.hagnarigny@gmail.com",
        to: req.body.email,
        subject: "Reinitialiser votre mot de passe",
        html: message,
    };

    transporter.sendMail(mailOptions, (err, infos) => {
        if (err) {
            console.log(err);
            req.flash("err", err.message);
        } else {
            console.log(infos);
            req.flash(
                "success",
                "Bravo, un message a été envoyé à votre adresse e-mail: " +
                req.body.email +
                ". Veuillez vérifier votre boite e-mail et cliquer sur le lier"
            );
            return res.redirect("/users/forgot-password");
        }
    });
};
module.exports = sendResetMail;